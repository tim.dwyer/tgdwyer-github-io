---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
Some content. Some more.
```typescript
const x:number = 2;
function fun<T>(o:number) {
    return <T>{x:blah}
}
```

[Observable Asteroids](asteroids)